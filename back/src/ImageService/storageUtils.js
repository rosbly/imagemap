'use strict';


function imageToBson(image) {
  const bson = {
    url: image.url,
    thumbnail: image.thumbnail,
  };

  if (image.longitude !== null && image.latitude !== null) {
    bson.location = [image.longitude, image.latitude];
  }

  return bson;
}


function bsonToImage(bson) {
  const image = {
    url: bson.url,
    thumbnail: bson.thumbnail,
  };

  if (bson.location) {
    image.longitude = bson.location[0];
    image.latitude = bson.location[1];
  }

  return image;
}


module.exports = {imageToBson, bsonToImage};
