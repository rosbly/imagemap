'use strict';

const MIN_LONGITUDE = -180;
const MAX_LONGITUDE = +180;
const MIN_LATITUDE = -90;
const MAX_LATITUDE = +90;

function isValidLongitude(lng) {
  return (
    typeof lng === 'number'
    && MIN_LONGITUDE <= lng && lng <= MAX_LONGITUDE
  );
}

function isValidLatitude(lat) {
  return (
    typeof lat === 'number'
    && MIN_LATITUDE <= lat && lat <= MAX_LATITUDE
  );
}

module.exports = {
  MIN_LONGITUDE,
  MAX_LONGITUDE,
  isValidLatitude,
  isValidLongitude
};
