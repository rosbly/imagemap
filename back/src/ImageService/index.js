'use strict';

const readImage = require('./readImage');
const buildQuery = require('./buildQuery');
const {bsonToImage, imageToBson} = require('./storageUtils');

const COLLECTION_NAME = 'images';


module.exports = class ImageService {
  constructor(db) {
    this._collection = db.collection(COLLECTION_NAME);
  }

  async addImage(url, buffer) {
    const image = await readImage(url, buffer);
    const bson = imageToBson(image);
    const result = await this._collection.replaceOne({url}, bson, {upsert: true});
    return result.upsertedId;
  }

  async findImages(bbox) {
    const query = buildQuery(bbox);
    const cursor = this._collection.find(query);
    const images = await cursor.toArray();
    return images.map(bsonToImage);
  }
};
