'use strict';

const Jimp = require('jimp');
const ExifParser = require('exif-parser');
const { isValidLongitude, isValidLatitude } = require('./locationUtils');
const { InvalidInputError } = require('../errors');

// TODO make API clearer about error handling: we are using not-that-well-defined libraries here


module.exports = async function readImage(url, buffer) {
  const [longitude, latitude] = readImageLocation(buffer);
  const thumbnail = await makeThumbnailDataUrl(buffer);

  return {
    url,
    longitude,
    latitude,
    thumbnail,
  };
};


function readImageLocation(imageBuffer) {
  const exifData = parseExifData(imageBuffer);
  const lng = exifData.tags['GPSLongitude'];
  const lat = exifData.tags['GPSLatitude'];

  return (
    isValidLongitude(lng) && isValidLatitude(lat)
      ? [lng, lat]
      : [null, null]
  );
}


function parseExifData(imageBuffer) {
  try {
    return ExifParser.create(imageBuffer).parse();
  } catch(err) {
    // ExifParser does not have special error reporting...so just catch & rewrap them all...
    throw new InvalidInputError(`Could not parse EXIF data: ${err.message}`);
  }
}


async function makeThumbnailDataUrl(imageBuffer, longestEdgeLength = 40) {
  const image = await Jimp.read(imageBuffer);

  if (image.bitmap.width < image.bitmap.height) {
    image.resize(Jimp.AUTO, longestEdgeLength);
  } else {
    image.resize(longestEdgeLength, Jimp.AUTO);
  }

  return getImageDataUrl(image);
}


function getImageDataUrl(image) {
  return new Promise(
    (resolve, reject) =>
      image.getBase64(
        Jimp.AUTO,
        (err, data) => err ? reject(err) : resolve(data)
      )
  );
}
