'use strict';

const assert = require('chai').assert;
const {bsonToImage, imageToBson} = require('./storageUtils');


describe("Image serialization to MongoDB format", () => {
  it("Should convert back and forth with no change", () => {
    const originalImage = {
      latitude: 1.25,
      longitude: -1.5,
      thumbnail: "some data",
      url: "some url",
    };

    const newImage = bsonToImage(imageToBson(originalImage));

    assert.deepEqual(newImage, originalImage);
  });
});
