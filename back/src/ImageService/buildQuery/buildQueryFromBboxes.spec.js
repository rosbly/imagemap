'use strict';

const assert = require('chai').assert;
const buildQueryFromBboxes = require('./buildQueryFromBboxes');


describe("buildQueryFromBboxes", () => {
  it("should make an empty query if no bbox is provided", function () {
    assert.deepEqual(
      buildQueryFromBboxes([]),
      {}
    );
  });

  it("should make a simple query when provided a single bbox", function () {
    const inputBboxes = [
      [0, 1, 2, 3]
    ];

    const expectedResult = {
      location: {
        $geoWithin: {
          $box : [ [0, 1], [2, 3] ]
        }
      }
    };

    assert.deepEqual(
      buildQueryFromBboxes(inputBboxes),
      expectedResult,
    );
  });

  it("should compose with $or when several bboxes are provided", function () {
    const inputBboxes = [
      [0, 1, 2, 3],
      [4, 5, 6, 7],
    ];

    const expectedResult = {
      $or: [
        {
          location: {
            $geoWithin: {
              $box : [ [0, 1], [2, 3] ]
            }
          }
        },
        {
          location: {
            $geoWithin: {
              $box : [ [4, 5], [6, 7] ]
            }
          }
        },
      ]
    };

    assert.deepEqual(
      buildQueryFromBboxes(inputBboxes),
      expectedResult,
    );
  });
});
