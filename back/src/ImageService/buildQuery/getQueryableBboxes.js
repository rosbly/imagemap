'use strict';

const { InvalidInputError } = require('../../errors.js');
const { isValidLongitude, isValidLatitude, MIN_LONGITUDE, MAX_LONGITUDE } = require('../locationUtils');


module.exports = function getQueryableBboxes(bbox) {
  const [lng1, lat1, lng2, lat2] = bbox;

  if (!isValidLatitude(lat1)
    || !isValidLatitude(lat2)
    || !isValidLongitude(lng1)
    || !isValidLongitude(lng2)
    || lat1 > lat2) {
    throw new InvalidInputError(`Invalid bbox coordinates: ${JSON.stringify(bbox)}`);
  }

  // Queries accross 180° meridian are allowed but must be split in two bboxes for MongoDB
  return (
    lng1 <= lng2
      ? [[lng1, lat1, lng2, lat2]]
      : [[lng1, lat1, MAX_LONGITUDE, lat2], [MIN_LONGITUDE, lat1, lng2, lat2]]
  );
};
