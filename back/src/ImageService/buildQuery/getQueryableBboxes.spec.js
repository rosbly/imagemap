'use strict';

const assert = require('chai').assert;
const getQueryableBboxes = require('./getQueryableBboxes');
const { InvalidInputError } = require('../../errors');


describe("getQueryableBboxes", function () {
  it("should return input as-is when longitudes are within [-180, 180) (nominal case)", function () {
    assert.deepEqual(
      getQueryableBboxes([-41, -42, 43, 44]),
      [[-41, -42, 43, 44]]
    );
  });

  it("should return 2 bboxes if input is an area around the 180° meridian", function () {
    assert.deepEqual(
      getQueryableBboxes([179, 10, -179, 20]),
      [ [179, 10, 180, 20], [-180, 10, -179, 20] ]
    );
  });

  it("should raise an error if some longitude is out of range", function () {
    assert.throws(
      () => getQueryableBboxes([-181, -90, 2, +90]),
      InvalidInputError
    );
  });

  it("should raise an error if some latitude is out of range", function () {
    assert.throws(
      () => getQueryableBboxes([0, -91, 2, +90]),
      InvalidInputError
    );
  });

  it("should raise an error if latitudes are not ordered", function () {
    assert.throws(
      () => getQueryableBboxes([0, 10, 2, 9]),
      InvalidInputError
    );
  });
});
