'use strict';


module.exports = function buildQueryFromBboxes(bboxes) {
  return make$or(
    bboxes
      .map(make$box)
      .map(make$geoWithin)
      .map(makeLocationFilter)
  );
};


function make$or(clauses) {
  switch (clauses.length) {
  case 0:
    return {};
  case 1:
    return clauses[0];
  default:
    return { $or: [...clauses] };
  }
}


function makeLocationFilter(clause) {
  return { location: clause };
}


function make$geoWithin(geometry) {
  return { $geoWithin: geometry };
}


function make$box(bbox) {
  const [lng1, lat1, lng2, lat2] = bbox;
  return {
    $box: [
      [lng1, lat1],
      [lng2, lat2],
    ]
  };
}
