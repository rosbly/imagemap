"use strict";

const getQueryableBboxes = require('./getQueryableBboxes');
const buildQueryFromBboxes = require('./buildQueryFromBboxes');

module.exports = function buildQuery(bbox) {
  return buildQueryFromBboxes(
    getQueryableBboxes(bbox)
  );
};
