'use strict';

const express = require('express');
const rc = require('rc');
const retry = require('imagemap-utils/retry');
const MongoClient = require('mongodb').MongoClient;

const logger = require('./logger');
const ImageService = require('./ImageService');
const makeImageRouter = require('./ImageRouter/makeImageRouter');
const { failHard, handleRequestError } = require('./errors');


const conf = rc('imagemapBack', {
  port: 3000,
  mongo: {
    host: 'localhost:27017',
    database: 'imagemap',
    username: 'imagemapUser',
    password: 'imagemapPassword',
    connectTimeoutSecond: 10,
  }
});

serve({port: conf.port, mongoOptions: conf.mongo})
  .catch(err => failHard("serve", err));


async function serve({port, mongoOptions}) {
  const mongoClient = await connectToMongo(mongoOptions);
  const imageService = new ImageService(mongoClient.db());

  const app = express();
  app.use(logger.expressLogger);
  app.use('/images', makeImageRouter(imageService));
  app.use(handleRequestError);
  await listen(app, port);

  logger.info("Server is ready");
}


async function connectToMongo({host, database, username, password, connectTimeoutSecond}) {
  const url = `mongodb://${username}:${password}@${host}/${database}`;
  const publicUrl = `mongodb://${username}:[password]@${host}/${database}`;

  logger.info(`Connecting to ${publicUrl}`);

  return retry({
    operation: () => MongoClient.connect(url),
    isTransientError: err => err.name === 'MongoNetworkError',
    retryCount: connectTimeoutSecond,
    retryIntervalMs: 1000,
  });
}


function listen(app, port) {
  return new Promise(function (resolve, reject) {
    try {
      app.listen(port, resolve());
    } catch(err) {
      reject(err);
    }
  });
}
