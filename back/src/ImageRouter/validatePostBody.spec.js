'use strict';

const assert = require('chai').assert;
const validatePostBody = require('./validatePostBody');
const { InvalidInputError } = require('../errors');


describe("ImageRouter.validatePostBody", function () {

  it("should work in nominal case", function () {
    assert.doesNotThrow(
      () => validatePostBody({url: "123"})
    );
  });

  it("should fail when given extra data", function () {
    assert.throws(
      () => validatePostBody({url: "123", extra: 123}),
      InvalidInputError
    );
  });

  it("should fail when url is not a string", function() {
    assert.throws(
      () => validatePostBody({url: 123}),
      InvalidInputError
    );
  });

  it("should fail when url is missing", function() {
    assert.throws(
      () => validatePostBody({}),
      InvalidInputError
    );
  });
});
