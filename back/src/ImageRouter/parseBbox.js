'use strict';

const { InvalidInputError } = require('../errors');


module.exports = function parseBbox(param) {
  const bbox = String(param)
    .split(',')
    .map(x => Number(x));

  if (!isBbox(bbox)) {
    throw new InvalidInputError(`bbox is required and must be 4 comma-separated numbers. Got ${JSON.stringify(param)}`);
  }
  return bbox;
};


function isBbox(bbox) {
  return (
    bbox instanceof Array
    && bbox.length === 4
    && bbox.every(Number.isFinite)
  );
}
