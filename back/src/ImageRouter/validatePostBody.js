'use strict';

const { InvalidInputError } = require('../errors');


module.exports = function validatePostBody(body) {
  if (typeof body !== "object"
    || typeof body.url !== "string"
    || Object.keys(body).length !== 1) {
    throw new InvalidInputError(
      `Arguments must be an object with an 'url' string attribute. Got ${JSON.stringify(body)}`
    );
  }
};
