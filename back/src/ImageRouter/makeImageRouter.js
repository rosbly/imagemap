'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');

const parseBbox = require('./parseBbox');
const validatePostBody = require('./validatePostBody');


module.exports = function makeImageRouter (service) {
  // Note that we can't subclass express.Router because it is not a constructor,
  // but a function that sets itself as the __proto__ of the object it builds
  const router = express.Router();

  router.get(
    '/',
    serveGet.bind(null, service)
  );

  router.post(
    '/',
    bodyParser.json(),
    servePost.bind(null, service)
  );

  return router;
};


function serveGet(service, req, res, next) {
  const bbox = parseBbox(req.query.bbox);

  service.findImages(bbox)
    .then(images => {
      res.json(images);
      next();
    })
    .catch(next);
}


function servePost(service, req, res, next) {
  validatePostBody(req.body);

  fetchBodyAsBuffer(req.body.url)
    .then(buffer => service.addImage(req.body.url, buffer))
    .then(imageId => {
      res.send(imageId);
      next();
    })
    .catch(next);
}


function fetchBodyAsBuffer(url) {
  return new Promise(
    (resolve, reject) => {
      const chunks = [];
      request(url)
        .on('error', err => reject(err))
        .on('data', chunk => chunks.push(chunk))
        .on('end', () => resolve(Buffer.concat(chunks)));
    }
  );
}
