'use strict';

const assert = require('chai').assert;
const { InvalidInputError } = require('../errors');
const parseBbox = require('./parseBbox');


describe('ImageRouter.parseBbox', function () {

  it("should return a bbox when given proper input", function () {
    assert.deepEqual(
      parseBbox('1,2,3,4.25'),
      [1, 2, 3, 4.25]
    );
  });

  it("should throw when given undefined", function () {
    assert.throws(
      () => parseBbox(undefined),
      InvalidInputError
    );
  });

  it("should throw when given three digits", function() {
    assert.throws(
      () => parseBbox('1,2,3'),
      InvalidInputError
    );
  });

  it("should throw when given five digits", function() {
    assert.throws(
      () => parseBbox('1,2,3,4,5'),
      InvalidInputError
    );
  });

  it("should throw when given extra characters", function() {
    assert.throws(
      () => parseBbox('1,2,3,4Hello'),
      InvalidInputError
    );
  });
});
