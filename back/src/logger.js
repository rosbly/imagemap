'use strict';

const winston = require('winston');
const expressWinston = require('express-winston');


const logger = new winston.Logger({
  level: getLoggingLevel(),
  transports: [
    new winston.transports.Console(),
  ]
});

function getLoggingLevel() {
  return (
    process.env.NODE_ENV === 'production'
      ? 'info'
      : 'debug'
  );
}


const expressLogger = expressWinston.logger({
  winstonInstance: logger,
  level: getRequestLogLevel,
});

function getRequestLogLevel(req, res) {
  return isSuccess(res.statusCode)
    ? 'debug'
    : 'info';
}

function isSuccess(status) {
  return 200 <= status && status < 300;
}


module.exports = logger;
module.exports.expressLogger = expressLogger;
