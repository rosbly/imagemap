'use strict';

const Bounce = require('bounce');
const logger = require('./logger');


class InvalidInputError extends Error {
  constructor(...args) {
    super(...args);
    this.name = 'InvalidInputError';
  }
}


// Final ExpressJS error handler
function handleRequestError(err, req, res, ignoredNext) {
  logger.warn(err);
  if (typeof err === 'object' && typeof err.statusCode === 'number') {
    // ExpressJS error as denoted by presence of statusCode attribute.
    // Note that we don't use error type because it may be for instance SyntaxError
    // because of the json middleware...what a mess
    res.status(err.statusCode).send(err.message || "");
  } else if(Bounce.isSystem(err)) {
    // Don't be as forgiving as ExpressJS default error handler: programming errors must lead to a crash
    failHard(`handling request ${req.url}`, err);
  } else {
    // Non-system error, non-express middleware error.
    if (err instanceof InvalidInputError) {
      res.status(400).send(err.message);
      // no next(err)
    } else {
      res.status(500).end();
    }
  }
}


function failHard(context, err) {
  logger.error(`${context}: ${err.message}`);
  logger.error(err.stack);
  process.exit(1);
}


process.on('uncaughtException', failHard.bind(null, "uncaughtException"));
process.on('unhandledRejection', failHard.bind(null, "unhandledRejection"));

module.exports = { InvalidInputError, handleRequestError };
