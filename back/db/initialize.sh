# Sourced by Mongo Dockerfile init script or executed by user on its environment
set -ex

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mongo \
    --host 127.0.0.1 \
    --port 27017 \
    --eval "var MONGO_INITDB_IMAGEMAP_PASSWORD='$MONGO_INITDB_IMAGEMAP_PASSWORD';" \
    "$MONGO_INITDB_DATABASE" \
    "$DIR"/initialize.js* # suffixed within container to avoid execution from mongo init script
