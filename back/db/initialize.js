conn = new Mongo();
if (!conn.getDBNames().includes("imagemap")) {
  db = conn.getDB("imagemap");
  db.createCollection("images");
  db.images.createIndex({"url": 1}, {"unique": true});
  db.images.createIndex({"location": "2d"});

  db.createUser({
    user: "imagemapUser",
    pwd: MONGO_INITDB_IMAGEMAP_PASSWORD, // meant as an environment variable, that must be imported via --eval due to mongo client limitation
    roles: [ "readWrite" ]
  });
}
