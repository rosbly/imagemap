'use strict';

const rc = require('rc');
const logger = require('./logger');
const crawl = require('./crawl');

const conf = rc('imagemapCrawler', {
  wikiUrl: 'https://en.wikipedia.org/w/api.php',
  backUrl: 'http://localhost:3000/images',
});

crawl(conf)
  .then(() => logger.info("All done, exiting"))
  .catch(err => {
    logger.error(err);
    process.exit(1);
  });
