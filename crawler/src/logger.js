'use strict';

const winston = require('winston');


const logger = new winston.Logger({
  level: getLoggingLevel(),
  transports: [
    new winston.transports.Console(),
  ]
});


function getLoggingLevel() {
  return (
    process.env.NODE_ENV === 'production'
      ? 'info'
      : 'debug'
  );
}

module.exports = logger;
