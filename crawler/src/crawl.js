'use strict';

const request = require('request-promise-native');
const MediaWikiImageCrawler = require('./mediaWiki/ImageCrawler');
const retry = require('imagemap-utils/retry');
const logger = require('./logger');


module.exports = async function crawl({wikiUrl, backUrl}) {
  const crawler = new MediaWikiImageCrawler(wikiUrl);

  let image = null;
  while((image = await getNextImage(crawler))) {
    if (image.longitude !== null && image.latitude !== null) {
      await tryPostImage(image, backUrl);
    }
  }
};


async function getNextImage(crawler) {
  return retry({
    operation: () => crawler.getNextImage(),
    isTransientError: isTransientRequestError,
    retryCount: 120,
    retryIntervalMs: 1000,
  });
}


async function tryPostImage(image, backUrl) {
  try {
    // Intended "return await" to catch exceptions from here
    return await retry({
      operation: () => {
        logger.info(`Posting ${image.url}`);
        return request({
          uri: backUrl,
          method: 'POST',
          json: true,
          body: { url: image.url },
        });
      },
      isTransientError: isTransientRequestError,
      retryCount: 120,
      retryIntervalMs: 1000,
    });
  } catch (err) {
    if (isRequestError(err)) {
      logger.warn(`Could not post image ${image.url}: ${err}`);
    } else { // programatic error, let it fail
      throw err;
    }
  }
}


function isRequestError(err) {
  return err.name === 'RequestError' || err.name === 'StatusCodeError';
}


function isTransientRequestError(err) {
  // Network errors or server errors not caused by our input
  return (
    err.name === 'RequestError'
    || (err.name === 'StatusCodeError' && !isClientErrorCode(err.statusCode))
  );
}


function isClientErrorCode(statusCode) {
  return /^4/.test(String(statusCode));
}
