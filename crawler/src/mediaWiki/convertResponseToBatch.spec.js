'use strict';

const assert = require('chai').assert;
const convertResponseToBatch = require('./convertResponseToBatch');

describe("convertResponseToBatch", function () {
  it("should handle empty responses", function () {
    const response = {};

    assert.deepEqual(
      convertResponseToBatch(response),
      {'continuation': null, 'images': []},
    );
  });

  it("should handle empty image sets in response", function () {
    const response = {
      query: {
        allimages: []
      }
    };

    assert.deepEqual(
      convertResponseToBatch(response),
      {'continuation': null, 'images': []},
    );
  });

  it("should extract continue.aicontinue as continuation", function () {
    const response = {
      continue: {
        aicontinue: 'foo',
      }
    };

    assert.deepEqual(
      convertResponseToBatch(response),
      {'continuation': 'foo', 'images': []},
    );
  });

  it("should extract images with and without GPS coordinages (simplified real-life example)", function() {
    const response = {
      batchcomplete: "",
      continue: {
        aicontinue: "White_certificates_diagram.gif",
        continue: "-||"
      },
      query: {
        allimages: [
          {
            name: "White_cat_in_Taguig.jpg",
            url: "https://upload.wikimedia.org/wikipedia/en/d/d5/White_cat_in_Taguig.jpg",
            descriptionurl: "https://en.wikipedia.org/wiki/File:White_cat_in_Taguig.jpg",
            descriptionshorturl: "https://en.wikipedia.org/w/index.php?curid=53954283",
            metadata: [
              {
                name: "Make",
                value: "Apple"
              },
              {
                name: "GPSLatitude",
                value: 14.535961111111
              },
              {
                name: "GPSLongitude",
                value: 121.05633611111
              },
              {
                name: "GPSAltitude",
                value: 19
              }
            ],
            ns: 6,
            title: "File:White cat in Taguig.jpg"
          },
          {
            name: "White_cell_with_eight_traversals_(spectroscopy).gif",
            url: "https://upload.wikimedia.org/wikipedia/en/0/0d/White_cell_with_eight_traversals_%28spectroscopy%29.gif",
            descriptionurl: "https://en.wikipedia.org/wiki/File:White_cell_with_eight_traversals_(spectroscopy).gif",
            descriptionshorturl: "https://en.wikipedia.org/w/index.php?curid=36224895",
            ns: 6,
            title: "File:White cell with eight traversals (spectroscopy).gif"
          }
        ]
      }
    };

    const expectedBatch = {
      continuation: "White_certificates_diagram.gif",
      images: [
        {
          url: "https://upload.wikimedia.org/wikipedia/en/d/d5/White_cat_in_Taguig.jpg",
          longitude: 121.05633611111,
          latitude: 14.535961111111,
        },
        {
          url: "https://upload.wikimedia.org/wikipedia/en/0/0d/White_cell_with_eight_traversals_%28spectroscopy%29.gif",
          longitude: null,
          latitude: null,
        },
      ]
    };

    assert.deepEqual(convertResponseToBatch(response), expectedBatch);
  });
});
