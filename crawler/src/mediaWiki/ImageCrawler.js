'use strict';

const fetchBatch = require('./fetchBatch');


// TODO multi-site ImageCrawler

/**
 * Utility to fetch all images from a given WikiMedia
 * TODO ES2018 & async* instead of this class
 */
module.exports = class ImageCrawler {
  /**
   * url is something like 'https://en.wikipedia.org/w/api.php'
   */
  constructor(url) {
    this._url = url;
  }

  /**
   * Returns next crawled image, or a null value if end has been reached
   */
  async getNextImage() {
    if (!this._batch) { // ie first call
      this._batch = await fetchBatch(this._url);
    }

    let image = this._batch.images.shift();

    if (!image && this._batch.continuation) {
      this._batch = await fetchBatch(this._url, this._batch.continuation);
      image = this._batch.images.shift();
    }

    return image || null;
  }
};
