'use strict';

module.exports = function buildQuery(url, startFile = null) {
  const components = [
    'action=query',
    'format=json',
    'list=allimages',
    'aiprop=metadata|url',
    'ailimit=500',
  ];

  if (startFile) {
    components.push(`aifrom=${encodeURIComponent(startFile)}`);
  }

  return url + '?' + components.join('&');
};
