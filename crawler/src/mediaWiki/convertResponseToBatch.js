'use strict';

const logger = require('../logger');
const navigateSafely = require('imagemap-utils/navigateSafely');


module.exports = function convertResponseToBatch(response) {
  return {
    continuation: navigateSafely(response, 'continue', 'aicontinue') || null,
    images: (navigateSafely(response, 'query', 'allimages') || []).map(convertImage),
  };
};


function convertImage(wikiImage) {
  const [longitude, latitude] = extractImageCoordinates(wikiImage);

  return {
    url: wikiImage.url,
    latitude,
    longitude,
  };
}


function extractImageCoordinates(image) {
  const exifProps = image.metadata || [];
  let longitude = null;
  let latitude = null;

  for(const prop of exifProps) {
    if (prop.name === 'GPSLongitude') {
      longitude = parseImageCoordinate(image, prop);
    } else if (prop.name ===  'GPSLatitude') {
      latitude = parseImageCoordinate(image, prop);
    }
  }

  return [longitude, latitude];
}


function parseImageCoordinate(image, prop) {
  if (typeof prop.value !== 'number') {
    logger.warn(`Coordinate ${prop.name} not a number for ${image.url}`);
    return null;
  }
  return prop.value;
}
