'use strict';

const assert = require('chai').assert;
const buildQuery = require('./buildQuery');


describe("buildQuery", function () {

  it("should build a query with no continuation argument", function () {
    assert.equal(
      buildQuery('https://en.wikipedia.org/w/api.php'),
      'https://en.wikipedia.org/w/api.php?action=query&format=json&list=allimages&aiprop=metadata|url&ailimit=500'
    );
  });

  it("should build a query with continuation, with proper URI escaping", function () {
    assert.equal(
      buildQuery('https://en.wikipedia.org/w/api.php', 'f oo'),
      'https://en.wikipedia.org/w/api.php?action=query&format=json&list=allimages&aiprop=metadata|url&ailimit=500&aifrom=f%20oo'
    );
  });
});
