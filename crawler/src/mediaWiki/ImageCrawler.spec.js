'use strict';

const assert = require('chai').assert;
const td = require('testdouble');


describe("ImageCrawler", function() {
  let fetchBatch;
  let ImageCrawler;

  beforeEach(function () {
    fetchBatch = td.replace('./fetchBatch');
    ImageCrawler = require('./ImageCrawler');
  });

  afterEach(function () {
    td.reset();
  });

  it("should return no image if API returns empty response", async function () {
    td.when(fetchBatch('some.url')).thenReturn({images: []});

    const crawler = new ImageCrawler('some.url');

    assert.equal(await crawler.getNextImage(), null);
  });

  it("should work with a single batch", async function () {
    td.when(fetchBatch('some.url')).thenReturn({images: ['image1', 'image2']});

    const crawler = new ImageCrawler('some.url');

    assert.equal(await crawler.getNextImage(), 'image1');
    assert.equal(await crawler.getNextImage(), 'image2');
    assert.equal(await crawler.getNextImage(), null);
  });

  it("should work with several batches", async function () {
    td.when(fetchBatch('some.url')).thenReturn({images: ['image1', 'image2'], continuation: 'foo'});
    td.when(fetchBatch('some.url', 'foo')).thenReturn({images: ['image3'], continuation: 'bar'});
    td.when(fetchBatch('some.url', 'bar')).thenReturn({images: ['image4']});

    const crawler = new ImageCrawler('some.url');

    assert.equal(await crawler.getNextImage(), 'image1');
    assert.equal(await crawler.getNextImage(), 'image2');
    assert.equal(await crawler.getNextImage(), 'image3');
    assert.equal(await crawler.getNextImage(), 'image4');
    assert.equal(await crawler.getNextImage(), null);
  });
});
