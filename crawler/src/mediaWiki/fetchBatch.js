'use strict';

const logger = require('../logger');
const buildQuery = require('./buildQuery');
const request = require('request-promise-native');
const convertResponseToBatch = require('./convertResponseToBatch');


module.exports = async function fetchBatch(url, start = null) {
  logger.debug(`Fetching MediaWiki batch from ${start} on ${url}`);

  const query = buildQuery(url, start);
  const json = await request({url: query, json: true});
  return convertResponseToBatch(json);
};
