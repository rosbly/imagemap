# ImageMap: an example Node.js application

Just a simple webapp crawling Wikipedia and displaying its images on a map:

![Screenshot](doc/screenshot.png)

It is aimed at new Node.js developers who want a starting point with a **preselection of tools and configurations for a complete project**.

Because even though Javascript is well covered by tutorials and books[^1], it is difficult and time-consuming to choose among the plethora of libraries and tools available.

>**Note:**
this project is *not* a general code design showcase, the problem solved being simplistic. It's more about integrating the patchwork of technologies of the JS world


## Project design

### Architecture

- front-end: static files, with page queries to the back-end
- back-end: REST API to store and retrieve images
- crawler: queries Wikipedia and feeds the back-end

> **Note:**
as a matter of simplicity all 3 components are in the same Git repository, though in a real-life situation it would probably be appropriate to split it up.


### Tools & Libraries used

The choices shown in the table below may not be the best ones, but because we chose only popular tools, they should be reasonable choices.
Also, what's interesting is not only the choice for each tool/row, but *which* tools are actually needed[^4].

| Tool                | Context    | Choice          | Why                                                                                            | Popular alternative        |
|:--------------------|------------|-----------------|------------------------------------------------------------------------------------------------|----------------------------|
| Package manager     | Node       | NPM             | Only popular choice                                                                            | None[^2]                   |
| Package manager     | Browser    | NPM             | NPM became popular on front-end too<br/>+ NPM everywhere is just simpler                       | Bower                      |
| Bundler             | Browser    | Webpack         | Feature-complete (dev server, hot reload, ...) so that other tools eg Gulp/Grunt aren't needed | Browserify, Systemjs       |
| Transpiler          | Browser    | Babel           | Only popular choice. And it just works                                                         | None[^2]                   |
| Linter              | All        | ESLint          | Complete, configurable & actively maintained                                                   | JSLint, JSHint             |
| Web framework       | Node       | ExpressJS       | ExpressJS is the oldest and most popular framework. There are many good alternatives though    | Hapi, and many others      |
| Logging library     | Node       | Winston         | Most popular choice, prints human-readable logs                                                | Log4js, Bunyan             |
| Configuration lib   | Node       | rc              | Simple, supports configuration file, command line, and environment variables right away        | Dotenv                     |
| Unit test lib       | All        | Mocha with Chai | Popular & modular                                                                              | Jasmine                    |
| Mock framework      | All        | testdouble      | Nice opinionated library, not-that-popular but cool[^3]. Sinon is the most popular tool        | Sinon                      |
| Test coverage tool  | All        | Istanbul        | Only popular choice, as simple as it can be to set up                                          | None[^2]                   |

Outside of the JS world, here are some other tools used by this project:
- MongoDB, the database used by the back-end
- Nginx, to serve the front-end
- Docker & `docker-compose` to integrate the full application
- `tini` as an init system within Docker containers so that Node.js properly responds to signals

### Testing

Unit tests are stored alongside their respective tested units (Golang-style), and can be run with a classic `npm test` in each component directory.
Some integration tests should be added later.


### Limitations

This project remains a demo project:
* Code structure is not the usual DAO, models and stuff, since this app does little logic. Anyway, the purpose of this project is integration, not code structure
* Back-end performance is bad. Issues:
    * no server side clustering
    * crawler `POST`s tend to preempt back-end time, leaving the front-end little room for its `GET`s
* Crawler is slow because purely sequential (no concurrent queries). We wouldn't want to abuse WikiMedia servers anyway
* Front-end is simplistic and does not use any framework
* `utils` local NPM module makes `Dockerfile`s clumsy. In an enterprise environment we could set up a local NPM repository to solve this


## Run it

ImageMap runs on a Linux system, locally or within Docker containers.

### Within docker

```bash
docker-compose up
```
Website is served at [http://localhost:8080]. New images will progressively appear as the crawler does its job (move the map to trigger new queries).

>Tested with Docker 18.03.1-ce, docker-compose 1.21.2, on Linux 4.4.0

### Locally

With a locally installed MongoDB instance:
```bash
cd imagemap/back
mongo --eval "var MONGO_INITDB_IMAGEMAP_PASSWORD='imagemapPassword';" db/initialize.js
```

Then run each component with an `npm start` from its respective directory: `back/`, then `crawler/` and `front/`. Note that `npm start` from `front/` runs the Webpack development server, not Nginx as does the front-end Docker container.


[^1]: for experienced programmers learning Javascript, I recommend [Axel Rauschmayer's series](http://exploringjs.com/), in particular [Speaking JavaScript](http://speakingjs.com/) and [Exploring ES6](http://exploringjs.com/es6.html). You can learn the basics very quickly, then go in-depth, all without loosing time with general programming concepts

[^2]: there are certainly alternatives out there, but they're not as nearly as popular as our choice

[^3]: opinionated with good arguments, see [this presentation](https://vimeo.com/257056050) on good mocking practices. Valuable advices for any language

[^4]: those choices are based on lots of articles and try outs, though one resource stands out, it's the excellent [Node.js Best Practices](https://github.com/i0natan/nodebestpractices)
