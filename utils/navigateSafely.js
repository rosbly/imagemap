'use strict';

// Safe navigation function (similar to safe navigation operators in eg Groovy or Ruby)
module.exports = function navigateSafely(obj, ...path) {
  for(const key of path) {
    if (obj !== Object(obj) || !(key in obj)) {
      return undefined;
    }
    obj = obj[key];
  }
  return obj;
};
