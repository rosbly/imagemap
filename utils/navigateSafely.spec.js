'use strict';

const assert = require('chai').assert;
const navigateSafely = require('./navigateSafely');


describe("utils.navigateSafely", function () {
  it("should go down to value pointing by provided path if it exists", function () {
    const b1 = 1;
    const b2 = [2, 3];
    const a = {b1, b2};
    const rootObj = {a};

    assert.deepEqual(navigateSafely(rootObj), rootObj);
    assert.deepEqual(navigateSafely(rootObj, "a"), a);
    assert.deepEqual(navigateSafely(rootObj, "a", "b2"), b2);
    assert.equal(navigateSafely(rootObj, "a", "b2", "0"), 2);
  });

  it("should work with non object values", function () {
    assert.equal(navigateSafely(123), 123);
    assert.equal(navigateSafely(null), null);
    assert.equal(navigateSafely(undefined), undefined);
  });

  it("should return undefined if path points to nothing", function () {
    assert.equal(navigateSafely(123, "does not exist"), undefined);
    assert.equal(navigateSafely({a: {b: 1}}, "nope"), undefined);
    assert.equal(navigateSafely({a: {b: 1}}, "a", "nope"), undefined);
  });
});
