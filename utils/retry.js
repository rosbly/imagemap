'use strict';


module.exports = async function retry({operation, isTransientError, retryCount, retryIntervalMs}) {
  checkOptions(arguments[0]);
  let retryIndex = 0;
  for(;;) {
    try {
      const result = await operation(); // intentional return + await since we want to catch exceptions from here
      return result;
    } catch(err) {
      if (isTransientError(err) && retryIndex < retryCount) {
        await sleep(retryIntervalMs);
        retryIndex += 1;
        // loop over
      } else {
        throw err;
      }
    }
  }
};

function checkOptions(options) {
  checkOption(options, 'operation', 'function');
  checkOption(options, 'isTransientError', 'function');
  checkOption(options, 'retryCount', 'number');
  checkOption(options, 'retryIntervalMs', 'number');
}

function checkOption(options, name, expectedType) {
  if (typeof options[name] !== expectedType) {
    throw new TypeError(`Argument ${name} to retry is expected to be a ${expectedType}, got ${options[name]}`);
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
