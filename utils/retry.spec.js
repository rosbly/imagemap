'use strict';

const assert = require('chai').assert;
const retry = require('./retry');


async function testRetry(...operationList) {
  return retry({
    operation: () => operationList.shift()(),
    isTransientError,
    retryCount: 1,
    retryIntervalMs: 0,
  });
}

function returnSuccess() {
  return "success";
}

function throwTransientError() {
  throw new Error("transient error");
}

function throwDefinitiveError() {
  throw new Error("definitive error");
}

function isTransientError(err) {
  return err.message === "transient error";
}


describe("utils.retry", async function () {

  it("should return result on first success", async function () {
    const result = await testRetry(returnSuccess);
    assert.equal(result, "success");
  });


  it("should return result on successful retry", async function() {
    const result = await testRetry(throwTransientError, returnSuccess);
    assert.equal(result, "success");
  });

  it("should let retryable exception pass through when retry count got exceeded", async function() {
    let err = null;
    try {
      await testRetry(throwTransientError, throwTransientError);
    } catch (e) {
      err = e;
    }
    assert.equal(err.message, "transient error");
  });


  it("should immediately throw in case of definitive error (on first try)", async function() {
    let err = null;
    try {
      await testRetry(throwDefinitiveError);
    } catch (e) {
      err = e;
    }
    assert.equal(err.message, "definitive error");
  });


  it("should throw in case of definitive error (on a retry)", async function() {
    let err = null;
    try {
      await testRetry(throwTransientError, throwDefinitiveError);
    } catch (e) {
      err = e;
    }
    assert.equal(err.message, "definitive error");
  });


  it("should throw TypeError when any mandatory argument is mistyped", async function() {
    await checkRetryThrowsTypeErrorOnMissingArg('operation');
    await checkRetryThrowsTypeErrorOnMissingArg('isTransientError');
    await checkRetryThrowsTypeErrorOnMissingArg('retryCount');
    await checkRetryThrowsTypeErrorOnMissingArg('retryInterval');
  });

  async function checkRetryThrowsTypeErrorOnMissingArg(missingArgName) {
    const args = {
      operation: () => "ok",
      isTransientError: () => false,
      retryCount: 2,
      retryInterval: 100,
    };
    args[missingArgName] = undefined;

    try {
      await retry(args);
      assert.fail(`An exception should have been thrown because of missing ${missingArgName}`);
    } catch (err) {
      assert.equal(err.name, "TypeError", `TypeError should have been thrown because of missing ${missingArgName}`);
    }
  }
});
