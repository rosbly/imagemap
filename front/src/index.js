// Messy imports until Leaflet.Photo becomes a proper NPM package (see #6)
import 'leaflet/dist/leaflet.css';
import '../Leaflet.Photo/Leaflet.Photo.css';

import L from 'leaflet/dist/leaflet.js';
import 'leaflet.markercluster';
import '../Leaflet.Photo/Leaflet.Photo.js';
import './style.css';


makeMap();


function makeMap() {
  const tileLayer = makeTileLayer();
  const photoLayer = makePhotoLayer();

  const map = L.map('mapid', {
    layers: [tileLayer, photoLayer],
    center: [20, 0],
    zoom: 3,
    minZoom: 3,
    maxBounds: L.latLngBounds([-90, -180], [90, 180]),
    closePopupOnClick: false,
  });

  refreshPhotoLayer(map, photoLayer);
  map.on('moveend', () => refreshPhotoLayer(map, photoLayer));

  return map;
}


function makeTileLayer() {
  const maxZoom = 18;
  const url = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  const attribution = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';

  return L.tileLayer(url, {attribution, maxZoom, noWrap: true});
}


function makePhotoLayer() {
  const layer = L.photo.cluster();
  layer.on(
    'click',
    evt => createPhotoPopup(evt.layer, evt.layer.photo.url)
  );
  return layer;
}


function createPhotoPopup(layer, url) {
  let popup = null;

  const a = document.createElement('a');
  a.className = 'imagemap-photo-popup';
  a.title = url;
  a.href = url;
  a.target = "_blank";

  const img = new Image();
  img.src = url;
  img.onload = () => popup.openPopup(); // open popup only once image size is known - otherwise it is misplaced
  a.appendChild(img);

  popup = layer.bindPopup(a);
}


function refreshPhotoLayer(map, photoLayer) {
  const bounds = clampLatLngBounds(map.getBounds());

  fetch(`/api/images?bbox=${bounds.toBBoxString()}`)
    .then(response => response.json())
    .then(photos => { photoLayer.clear(); photoLayer.add([...photos].map(convertPhoto)); })
    .catch(err => console.error(err));
}


function clampLatLngBounds(b) {
  return L.latLngBounds(
    clampLatLng(b.getSouthWest()),
    clampLatLng(b.getNorthEast()),
  );
}


function clampLatLng(v) {
  return L.latLng(
    clamp(-90, v.lat, +90),
    clamp(-180, v.lng, +180),
  );
}


function clamp(min, value, max) {
  return Math.min(Math.max(value, min), max);
}


function convertPhoto(photo) {
  return {
    lat: photo.latitude,
    lng: photo.longitude,
    url: photo.url,
    thumbnail: photo.thumbnail,
    caption: photo.url,
  };
}
