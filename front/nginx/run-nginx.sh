#!/bin/bash
set -e

if [[ $# -ne 1 ]]; then
    echo "Backend url argument is mandatory (eg. 'http://localhost:3000/')" >&2
    exit 1
fi

BACKURL="$1"
sed "s@BACKURL@${BACKURL}@g" /server.conf.template > /etc/nginx/conf.d/server.conf
exec nginx -g 'daemon off;'
